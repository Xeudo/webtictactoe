$(document).ready(function () {
  // Variable qui défini qui es le joueur
  let joueur = 'x';

  // variable qui décrit les état de jeu
  let partieEnCours = true;

  // variable qui définit les scores
  let scoreX = 0,
    scoreO = 0;

  // tableau qui défénit tour par tour les emplacé cocher et ce par quel joueurs.
  let tableau = ["", "", "", "", "", "", "", "", ""];

  // Tableau des conditions de victoire
  const conditionVictoire = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];

  // permet de désactiver le popup de fin de partie
  $(".pop-bg").hide();

  // Fonction cliquer sur une case, permet de mettre un X ou un O selon le joueur
  $(".case").click(function () {
    if (!($(this).hasClass("cocherX") || $(this).hasClass("cocherO")) && partieEnCours == true) {
      if (joueur == "x") {
        $(this).addClass("cocherX");
        tableau[$(this).index()] = joueur;
      } else {
        $(this).addClass("cocherO");
        tableau[$(this).index()] = joueur;
      }
      joueur = (joueur == 'x') ? 'o' : 'x';
      changementCouleur();
      $("#joueurAffichage").html(joueur.toUpperCase());
      verifierCondition();

    }
  });

  // permet de changer les couleurs du site internet en fonction du joueur qui doit jouer.
  function changementCouleur() {
    if (joueur == "x") {
      $(".resetButton,.title,table").removeClass("bleu").addClass("rouge");
      $(".resetButton").mouseover(function () {
        $(this).css("border", "5px solid #ff615f");
      }).mouseleave(function () {
        $(this).css("border", "none");
      });
    } else {
      $(".resetButton,.title,table").addClass("bleu").removeClass("rouge");
      $(".resetButton").mouseover(function () {
        $(this).css("border", "5px solid #3ec5f3");
      }).mouseleave(function () {
        $(this).css("border", "none");
      });
    }
  }


  // permet de verifier si la partie est gagné
  // Vérifie le tableau qu'ont remplis (la variable tableau) avec des ["x" "" "o"]
  // par la suite boucle l'ensemble des rangé afin de vérifier si une combinaison gagnant est trouvé ==> (Les combinaison gagnante son stocker dans la variable conditionVictoire)
  function verifierCondition() {
    for (let i = 0; i <= 8; i++) {
      const poo = conditionVictoire[i]; // ex x x x
      let a = tableau[poo[0]];
      let b = tableau[poo[1]];
      let c = tableau[poo[2]];

      if (a === "" || b === "" || c === "") {
        continue;
      } else if (a === b && b === c) {
        messageFin();
        partieEnCours = false;
        augmenterScore(a);

        if (a == "x") {
          $("h1").html("Le joueur X vient de gagner la manche.");
        } else {
          $("h1").html("Le joueur O vient de gagner la manche.");

        }
      } else if (!tableau.includes("")) {
        messageFin();
        $("h1").html("Aucun joueur a gagné, la partie est nulle.");
        return;
      }
    }

  }

  // Augmente le pointage du joueur X de 1
  function augmenterX() {
    scoreX = scoreX + 1;
  }

  // Augmente le pointage du joueur O de 1
  function augmenterO() {
    scoreO = scoreO + 1;
  }

  // Actualise le pointage dans le site internet en fonction des variable scoreO-X
  function augmenterScore(joueur) {
    if (joueur == "x") {
      augmenterX();
      $("#pointageX").html(scoreX.toString());
    } else if (joueur == "o") {
      augmenterO();
      $("#pointageO").html(scoreO.toString());
    }
  }

  // Fonction qui reset la grille
  $(".resetButton").click(function () {
    $(".case").each(function () {
      if ($(".case").hasClass("cocherX") || $(".case").hasClass("cocherO")) {
        $(".case").removeClass("cocherX cocherO");
        joueur = "x"
        changementCouleur();
        $("#joueurAffichage").html(joueur.toUpperCase());
        tableau = ["", "", "", "", "", "", "", "", ""];
        partieEnCours = true;
      }
    })
  })
});

// Aparation du popup qui dit l'état de partie.
function messageFin() {
  $(".pop-bg").fadeIn(450);
  $(".popButton").click(function () {
    $(".pop-bg").fadeOut(450);
  });

}